import numpy as np
import numpy.linalg as la
import os
import os.path
import time

from optparse import OptionParser

ALLOWED_SAMPLING_RATES = [1, 2, 3, 5, 10, 20, 40, 80]
FNAMES=["in_magn_x_raw", "in_magn_y_raw", "in_magn_z_raw"]

def read_single_value(f):
    f.seek(0)
    return int(f.readline().strip("\n"))

def set_minmax(x_val, y_val, z_val, minmax_data):
    """check if a new min/max has been found and record all relevant data"""

    data = {
        "x" : x_val,
        "y" : y_val,
        "z" : z_val
    }
    components = ["x", "y", "z"]
    # init
    if len(minmax_data.keys()) == 0:
        minmax_data["xmin"] = minmax_data["xmax"] = x_val
        minmax_data["ymin"] = minmax_data["ymax"] = y_val
        minmax_data["zmin"] = minmax_data["zmax"] = z_val

    else:
        for comp in components:
            minmax_data[comp + "min"] = min(minmax_data[comp + "min"], data[comp])
            minmax_data[comp + "max"] = max(minmax_data[comp + "max"], data[comp])

def print_single_minmax(data):
    vector = np.array([
        data["x"],
        data["y"],
        data["z"]
    ])
    print('Name: {} x: {:.2f} y: {:.2f} z: {:.2f} length: {:.2f}'.format(
        data["name"], data["x"], data["y"], data["z"], la.norm(vector)))

def print_absolute_minmax(data):
    print('Global minima/maxima:')
    print('xmin {:.2f} xmax {:.2f} ymin {:.2f} ymax {:.2f} zmin {:.2f} zmax {:.2f}'.format(
        data["xmin"], data["xmax"], data["ymin"], data["ymax"], data["zmin"], data["zmax"]))

def run_loop(f_x, f_y, f_z, N, t, print_corrected=True):
# measurement data
    x_vals = np.zeros((N,))
    y_vals = np.zeros((N,))
    z_vals = np.zeros((N,))

    # min/max data
    minmax_data = {}

    # correction
    offset_x = 0
    offset_y = 0
    offset_z = 0

    delta_x = 0
    delta_y = 0
    delta_z = 0

    scale_x = 1
    scale_y = 1
    scale_z = 1

    # misc
    conv_helper_array = np.ones((N,)) / N

    # loop control
    quit = False
    i = 0

    use_running_average = True if N > 1 else False

    valid = not use_running_average


    print('Starting measurements. Press Ctrl-C to quit')

    while not quit:
        try:
            if i == N:
                i = 0
                if not valid:
                    print('\nGot enough samples. Printing running averages')
                    valid = True

            x_vals[i] = read_single_value(f_x)
            y_vals[i] = read_single_value(f_y)
            z_vals[i] = read_single_value(f_z)

            # get the running average
            x_avg = np.convolve (x_vals, conv_helper_array, "valid")[0]
            y_avg = np.convolve (y_vals, conv_helper_array, "valid")[0]
            z_avg = np.convolve (z_vals, conv_helper_array, "valid")[0]

            x_corrected = (x_avg - offset_x) * scale_x
            y_corrected = (y_avg - offset_y) * scale_y
            z_corrected = (z_avg - offset_z) * scale_z

            # only print once we have enough samples
            if valid:
                if print_corrected is True:
                    print('\rx {:.2f} y {:.2f} z {:.2f}      '.format(x_corrected, y_corrected, z_corrected), end='')
                else:
                    print('\rx {:.2f} y {:.2f} z {:.2f}      '.format(x_avg, y_avg, z_avg), end='')
                set_minmax (x_avg, y_avg, z_avg, minmax_data)
            else:
                print('\rGathering samples...', end='')

            # Update deltas
            offset_x = (minmax_data["xmax"] + minmax_data["xmin"]) / 2
            offset_y = (minmax_data["ymax"] + minmax_data["ymin"]) / 2
            offset_z = (minmax_data["zmax"] + minmax_data["zmin"]) / 2

            delta_x = (minmax_data["xmax"] - minmax_data["xmin"]) / 2
            delta_y = (minmax_data["ymax"] - minmax_data["ymin"]) / 2
            delta_z = (minmax_data["zmax"] - minmax_data["zmin"]) / 2

            avg_delta = (delta_x + delta_y + delta_z ) / 3

            if delta_x > 0 and delta_y > 0 and delta_z > 0:
                scale_x = avg_delta / delta_x
                scale_y = avg_delta / delta_y
                scale_z = avg_delta / delta_z

            i += 1
            time.sleep(t)

        except KeyboardInterrupt:
            print ("")
            print ("Offsets: x {} y {} z {}".format(offset_x, offset_y, offset_z))
            print ("Weights: x {} y {} z {}".format(scale_x, scale_y, scale_z))
            quit = True

    print('Finished')

    return minmax_data

def main():
    # Parse options
    parser = OptionParser()
    parser.add_option("-d", "--directory",
                      action="store", type="string", dest="directory",
                      help="Directory for iio sensor (magnetometer)")
    parser.add_option("-w", "--window-size",
                      action="store", type="int", dest="window_size",
                      help="Window size for the running average")
    parser.add_option("-t", "--time",
                      action="store", type="float", dest="time",
                      help="Time to wait between measurements. Remember to set a sensible sampling rate")
    parser.add_option("-s", "--sampling-rate",
                      action="store", type="int", dest="sampling_rate")

    parser.set_defaults(
        directory=None,
        window_size=1,
        time=0.05,
        sampling_rate=None,
        guided_mode=False)

    (options, args) = parser.parse_args()

    if options.directory is None:
        options.directory = os.getcwd()

    for f in FNAMES:
        if os.path.exists(os.path.join(options.directory, f)) is False:
            print('Error! File {} not found in directory'.format(f, options.directory))
            exit(1)

    if options.sampling_rate is not None:
        if options.sampling_rate not in ALLOWED_SAMPLING_RATES:
            print('Error! Sample rate not allowed')
            exit(1)

        if os.getuid() != 0:
            print('Error! Need to be root to set the sampling frequency')
            exit(1)

        print('Error. Setting sampling rate not yet implemented')
        exit(1)

    # Main Loop starts here
    N = options.window_size
    t = options.time
    dir = options.directory
    guided_mode = options.guided_mode

    # file handles
    f_x = open(os.path.join(dir, FNAMES[0]), 'r')
    f_y = open(os.path.join(dir, FNAMES[1]), 'r')
    f_z = open(os.path.join(dir, FNAMES[2]), 'r')

    if not guided_mode:
        minmax_data = run_loop(f_x, f_y, f_z, N, t)

    print_absolute_minmax(minmax_data)

    # clean up
    f_x.close()
    f_y.close()
    f_z.close()

if __name__ == '__main__':
    main()
